import { useContext } from 'react';
import {Nav, Navbar, NavDropdown} from 'react-bootstrap';
import { NavLink } from 'react-router-dom';
import UserContext from '../UserContext';

export default function Appnavbar() {

	const { user } = useContext(UserContext);
	return (
		<Navbar bg="primary" expand="lg" variant="dark">
			<Navbar.Brand href="/">
				StrawHAT
			</Navbar.Brand>
			<Navbar.Toggle aria-controls="basic-navbar-nav"/>
			<Navbar.Collapse id="basic-navbar-nav">
				<Nav className="me-auto">
					<Nav.Link as ={NavLink} to="/">Home</Nav.Link>

					{(user.id && user.isAdmin === true) ?
						<Nav>
							<Nav.Link as ={NavLink} to="/admin">Admin</Nav.Link>
							<NavDropdown title={user.firstName}>
								<NavDropdown.Item as={NavLink} to="/logout">Logout</NavDropdown.Item>
							</NavDropdown>
						</Nav>
						:
						(user.id && user.isAdmin === false) ?
							<Nav>
								<NavDropdown title={user.firstName}>
									<NavDropdown.Item as={NavLink} to="/logout">Logout</NavDropdown.Item>
								</NavDropdown>
							</Nav>
							:
							<Nav>
								<Nav.Link as ={NavLink} to="/register">Register</Nav.Link>
								<Nav.Link as ={NavLink} to="/login">Login</Nav.Link>
							</Nav>
					}
						
				</Nav>
				
				
			</Navbar.Collapse>
		</Navbar>
	)
}