import {useState, useEffect} from 'react';
import Swal from 'sweetalert2';
import {Container, Row, Col, Button, Table, Card} from 'react-bootstrap';


export default function Products() {
	const [products, setProducts] = useState([]);
	const [total, setTotal] = useState(0);
	const [inActive, setInActive] = useState(0);
	const [active, setActive] = useState(0);

	function editProduct() {
		Swal.fire({
				title: `Hi! This is under development`,
				icon: 'info',
				text: 'Available Soon!'
			})
	}

	useEffect(() => {
		fetch('https://morning-bastion-91045.herokuapp.com/products/allProducts')
		.then(outcome => outcome.json())
		.then(lists => {
			setProducts(lists.map((item)=> {

				return (
						
						<tr key={item._id}>
	        				<td>{item._id}</td>
	        				<td>{item.name.toUpperCase()}</td>
	        				<td>{item.description.toUpperCase()}</td>
	        				<td>{item.price}</td>
	        				<td>{String(item.isActive).toUpperCase()}</td>
	        				<td>
								<Button className="btn btn-primary" type="submit" id="submitBtn" onClick={() => editProduct()}>Edit</Button>
							</td>
      					</tr>
				)
			}))
			setTotal(lists.length);
			
		})
	});

	return (
		<Container>
			<div>
				<h3>Total Records: {total}</h3>
			</div>
			<div>				
				<Row className="mt-5">
					<Col>
						<Table bordered hover xs={12} md={4} responsive="sm" className="tableDesign">
							<thead >
								<tr >
									<th>Product ID</th>
									<th>Product Name</th>
									<th>Product Description</th>
									<th>Price</th>
									<th>Is Active?</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								{products}
							</tbody>
						</Table>
					</Col>
				</Row>
			</div>
		</Container>
	)
}