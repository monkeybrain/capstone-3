import Hero from '../components/Banner';
import {Container, Carousel} from 'react-bootstrap';

let introDetails = {
	title: "Straw Hat Crew",
	description: "This Ecommerce app will help you shop for the best Axie TEAMS",
	callToAction: `We build it, You play it!`
}

export default function Home() {
	return (
		<Container className="mt-5">
			<Hero intro={introDetails}/>
			<Carousel>
				<Carousel.Item>
					<img className="d-block w-100" src="bute.jpg" />
				</Carousel.Item>
				<Carousel.Item>
					<img className="d-block w-100" src="bute.jpg" />
				</Carousel.Item>
				<Carousel.Item>
					<img className="d-block w-100" src="bute.jpg" />
				</Carousel.Item>
			</Carousel>
		</Container>
	)
}