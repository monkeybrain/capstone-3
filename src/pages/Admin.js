import {useState, useEffect} from 'react';
import Hero from '../components/Banner';
import Swal from 'sweetalert2';
import {Container, Row, Col, Button, Table} from 'react-bootstrap';
import ProductList from '../components/Table';
import AddProduct from '../components/AddProduct';

const infoDetails = {
	title: "Welcome to Admin Page",
	description: "Admin is the role with the highest level of access to this app. You can add, delete,a pprove and deny edits.",
	callToAction: "Check the dashboard now!"
}



export default function Admin() {

	return (
		<Container>
			<Hero intro={infoDetails} />
			<AddProduct />
			<ProductList />
		</Container>
	)
}