import { useState, useEffect} from 'react';
import Hero from '../components/Banner';
import {Container, Form, Button} from 'react-bootstrap';
import { Redirect, useHistory } from 'react-router-dom';
import Swal from 'sweetalert2';


const introDetails = {
	title: "Create your own account here!",
	description: "This page will help you create your own account and enjoy the benefits of this app.",
	callToAction: "Register Now!"
}


export default function Register() {


	const history = useHistory();
	const [ firstName, setFirstName  ] = useState('');
	const [ middleName, setMiddleName ] = useState('');
	const [ lastName, setLastName ] = useState('');
	const [ email, setEmail ] = useState('');
	const [ password1, setPassword1 ] = useState('');
	const [ password2, setPassword2 ] = useState('');

	//effects
	const [ isRegBtnActive, setIsRegBtnActive ] = useState(false);
	const [ isPasswordMatch, setIsPasswordMatch ] = useState(false);
	const [ isComplete, setIsComplete ] = useState(false);

	function registerUser(event) {
		event.preventDefault();

		fetch('https://nameless-atoll-43324.herokuapp.com/users/register', {
			method: "POST",
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				firstName: firstName,
				middleName: middleName,
				lastName: lastName,
				email: email,
				password: password1
			})
		})
		.then(res => res.json())
		.then(data => {
			Swal.fire({
				title: `Hi! Your account is registered successfully`,
				icon: 'success',
				text: 'You may now login your account!'
			})
			history.push('/login');
		})

	}

	useEffect( () => {
		if ((password1 !== '' && password2 !== '') && password1 === password2) {
			setIsPasswordMatch(true);
			if ((firstName !== '' && middleName !== '' && lastName !== '' && email !== '' && password1 !=='' && password2 !== '') && (password1 === password2)) {
				setIsRegBtnActive(true);
				setIsComplete(true);
			}
		} else {
			setIsRegBtnActive(false);
			setIsPasswordMatch(false);
			setIsComplete(false);
		}
	}, [firstName, middleName, lastName, email, password1, password2])

	return (
		<Container className="mt-5">
			<Hero intro={introDetails}/>

			<Form onSubmit={(event) => registerUser(event)} className="mb-5 registerDesign p-5">
				{isComplete ?
					<h3 className="text-success mb-4">Click the Button to Register</h3>
					:
					<h3 className="text-primary mb-4">Sign-up</h3>
				}
				<Form.Group controlId="firstName">
					<Form.Label>First Name:</Form.Label>
					<Form.Control type="text" placeholder="Enter your First Name here" value={firstName} onChange={e => setFirstName(e.target.value)} required />
				</Form.Group>

				<Form.Group controlId="middleName">
					<Form.Label>Middle Name:</Form.Label>
					<Form.Control type="text" placeholder="Enter your Middle Name here" value={middleName} onChange={e => setMiddleName(e.target.value)} required />
				</Form.Group>

				<Form.Group controlId="lastName">
					<Form.Label>Last Name:</Form.Label>
					<Form.Control type="text" placeholder="Enter your Last Name here" value={lastName} onChange={e => setLastName(e.target.value)} required />
				</Form.Group>

				<Form.Group controlId="email">
					<Form.Label>Email Address:</Form.Label>
					<Form.Control type="email" placeholder="Enter your Email Address here" value={email} onChange={e => setEmail(e.target.value)} required />
				</Form.Group>

				<Form.Group controlId="password1">
					<Form.Label>Password:</Form.Label>
					<Form.Control type="password" placeholder="Enter your desired Password here" value={password1} onChange={e => setPassword1(e.target.value)} required />
				</Form.Group>

				{isPasswordMatch ?
					<p className="text-success">*** Password is matched!</p>
					:
					<p className="text-danger">*** Password should match!</p>
				}

				<Form.Group controlId="password2">
					<Form.Label>Confirm Password:</Form.Label>
					<Form.Control type="password" placeholder="Confirm your Password here" value={password2} onChange={e => setPassword2(e.target.value)} required />
				</Form.Group>

				{isRegBtnActive ?
					<Button variant="success" className="btn btn-block mt-3" type="submit" id="submitBtn"  >Create New Account</Button>
					:
					<Button variant="primary" className="btn btn-block mt-3" type="submit" id="submitBtn" disabled >Create New Account</Button>
				}

				
			</Form>
		</Container>
		
	)
}