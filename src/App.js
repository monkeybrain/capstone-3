//import React from 'react';
import { useEffect, useState } from 'react';
import './App.css';

import Navbar from './components/AppNavbar';
import Landing from './pages/Home';
import Admin from './pages/Admin';
import Error from './pages/Error';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';

import { Route, Switch, BrowserRouter as Router } from 'react-router-dom';
import { UserProvider } from './UserContext';


export default function App () {

  const [user, setUser] = useState({
    id: null,
    firstName: null,
    isAdmin: null
  });

  const unsetUser = () => {
    localStorage.clear();

    setUser({
      id: null,
      firstName: null,
      isAdmin: null
    })
  }

  useEffect(()=> {
    fetch('https://morning-bastion-91045.herokuapp.com/users/details', {
      headers: {
        Authorization: `Bearer ${ localStorage.getItem('accessToken') }`
      }
    })
    .then(resultOfQuery => resultOfQuery.json())
    .then(result => {
      if (result._id !== "undefined") {
        setUser({
          id: result._id,
          firstName: result.firstName,
          isAdmin: result.isAdmin
        })
      } else {
        setUser({
          id: null,
          firstName: null,
          isAdmin: null
        })
      }
    })
  },[])

  return (

    <UserProvider value={{user, unsetUser, setUser}}>
      
    	<Router>
    		<Navbar />
    		<Switch>
    			<Route exact path="/" component={Landing} />
    			<Route exact path="/register" component={Register} />
    			<Route exact path="/login" component={Login} />
    			<Route exact path="/admin" component={Admin} />
          <Route exact path="/logout" component={Logout} />
    			<Route component={Error} />
    		</Switch>
    	</Router>

    </UserProvider>


    );
}